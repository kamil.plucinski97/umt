### Preparation
1. Download files from `https://drive.man.poznan.pl/s/MxybgqMiNLpsWCa/download` and unpack `tar -xzvf "input.tar.gz"` or use `./download_data.sh` script. Around 9 GB of space is required.
2. Set variables `PROJECT_NAME="UMT" && IMAGE_TAG="<image_tag>"`

The output is usually in `accuracy time (seconds)` format. For initialization there is third column with `cost`.

### Reproduced Experiments
##### Table 1: Results reported in the original paper and those obtained by our implementation on the dataset of (Zhang et al.,2017). 
```bash
    docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/reproduce_zhang.py
```

##### Table 2: Results reported in the original paper and those obtained by our implementation on the dataset of (Dinu et al.,2014) with extensions of (Artetxe et al., 2017; Artetxe et al., 2018).
```bash
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/reproduce_paper.py
```

##### Table 3: Ablation test. Each subsequent command generates results for subsequent rows of the table:
    
Unsupervised initialization 
```bash 
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/without_init.py
```

Stochastic 
```bash 
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/stochastic.py
```

Cutoff (it requires ~500h of processing using GPU) 
```bash 
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/cutoff.py
```

CSLS 
```bash 
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/csls.py
```

Bidirectional 
```bash 
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/bidirectional.py
```

Re-weighting 
```bash 
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/whitening.py
```


### A closer look on method’s properties
##### Table 4: Results with various initialization dictionary sizes.
```bash
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/init_sizes.py
``` 

##### Table 5: The method’s performance with different minimal improvement threshold parameter.
```bash
 docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/reproduce_paper_threshold.py
```

##### Table 6: The method’s performance on different word embedding representations and on new pairs of languages.
First row for this table was generated in Table 2.
For second row that contains word2vec results for new languages:
```bash
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/new_languages.py
```

fastText results (for both rows):
```bash
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/fasttext.py
```

##### Table 7: Comparision of method’s performance with different initialization strategies. 
```bash
docker run -ti --rm --name=$PROJECT_NAME -v ${PWD}/input:/umt/data -v ${PWD}/output/datasets:/umt/output/datasets -v ${PWD}/output/tables_and_plots:/umt/output/tables_and_plots $IMAGE_TAG python3 benchmarks/initialization.py
``` 