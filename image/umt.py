import os

import click

from scripts.generate_arguments_combination import generate_arguments_combination
from scripts.validate import validation
from src.utils.config import Config
from scripts.create_supervised_validation_data import create_supervised_validation_data
from scripts.extract_lines_from_file import extract_lines_from_file
from src.utils.handling_data import get_validation_data, load_vectors
from scripts.translate import translate_function
from scripts.unsupervised_learning import unsupervised_learning

import numpy

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=CONTEXT_SETTINGS)
def cli():
    pass


@cli.command()
@click.option('-s', '--source', type=click.Path(exists=True), required=True, help='source embeddings path')
@click.option('-t', '--target', type=click.Path(exists=True), required=True, help='target embeddings path')
@click.option('-v', '--validation', type=click.Path(), required=False, default=None, help='path to parallel corpora')
@click.option('--reverse/--no-reverse', required=False, show_default=True, default='False',
              help='validation dict should be reversed')
@click.option('-es', '--embedding-size', type=int, required=False, show_default=True, default=200000,
              help='Size of embedding to load')
@click.option('-wi', '--words-initialize', type=int, required=False, show_default=True, default=4000,
              help='quantity of word used to initialize solution')
@click.option('-wl', '--words-learn', type=int, required=False, show_default=True, default=20000,
              help='quantity of word used to train')
@click.option('-dw', '--dropout-wait', type=int, required=False, show_default=True, default=50,
              help='number of iteration without improve after which it will be used dropout')
@click.option('-k', '--knn', type=int, required=False, show_default=True, default=10, help='k in kNN ;)')
@click.option('-kp', '--keep-probability', type=float, required=False, show_default=True, default=0.1,
              help='keep probability during first dropout')
@click.option('-o', '--output', type=click.Path(exists=True, file_okay=False, dir_okay=True), required=False,
              show_default=True, default=os.getcwd(), help='directory (not filepath) where output will be stored')
@click.option('-io', '--init-only', is_flag=True, required=False, show_default=True, default=False,
              help='you want to start only initialization without learning')
@click.option('-i', '--init',
              type=click.Choice(
                  ['classic', 'genetic', 'steepest', 'random', 'greedy', 'greedy-random', 'second', 'qap']),
              show_default=True, default='classic', required=False, help='Algorithm used to initialize solution')
@click.option('-it', '--init-time', type=int, show_default=True, default=3600, required=False,
              help='Initialization time in seconds only for local search and genetic')
@click.option('-c', '--cost', type=click.Choice(['classic', 'percentage', 'quantity']), show_default=True,
              default='classic', required=False, help='Way of calculating cost')
@click.option('-ct', '--cost-threshold', type=float, show_default=True,
              default='100', required=False, help='Percentages or quantity fist values in row to calculate cost')
@click.option('--tag', type=str, show_default=True, default=None, required=False,
              help='Tag will be presented in results.csv')
@click.option('-se', '--save-embeddings', is_flag=True, required=False, show_default=True, default=False,
              help='you want to save embeddings in cross-lingual space after learning')
@click.option('-so', '--save-output', is_flag=True, required=False, show_default=True, default=False,
              help='you want to save output after learning')
def train(**kwargs):
    """will learn to translate from provided embeddings"""
    config = Config(**kwargs)
    if config.quantity_words_to_initialize > config.quantity_words_to_learn:
        raise ValueError('Initialize dict shouldnt be bigger than learn dict')

    unsupervised_learning(config)


@cli.command()
@click.option('-xwx', type=click.Path(exists=True), required=False, help='path to source embeddings')
@click.option('-zwz', type=click.Path(exists=True), required=False, help='path to target embeddings')
@click.option('-v', '--validation-dict-path', type=click.Path(), required=False, default=None,
              help='path to parallel corpora')
@click.option('--reverse/--no-reverse', required=False, show_default=True, default='False',
              help='validation dict should be reversed')
@click.option('-es', '--embedding-size', type=int, required=False, show_default=True, default=200000,
              help='Size of embedding to load')
@click.option('-k', '--knn', type=int, required=False, default=10, help='k in kNN, if 0 translate without CSLS')
def validate(xwx, zwz, validation_dict_path, reverse, embedding_size, knn):
    """is responsible for data validation"""
    XWx_dict = load_vectors(xwx, embedding_size)
    ZWz_dict = load_vectors(zwz, embedding_size)
    validation(XWx_dict, ZWz_dict, validation_dict_path, reverse, knn)
    pass


@cli.command()
@click.option('-x', type=click.Path(exists=True), required=False, help='path to source embeddings')
@click.option('-z', type=click.Path(exists=True), required=False, help='path to target embeddings')
@click.option('-xz', type=click.Path(exists=True), required=False, help='path to source-target embeddings pairs')
@click.option('-wx', type=click.Path(exists=True), required=True, help='path to Wx matrix')
@click.option('-wz', type=click.Path(exists=True), required=True, help='path to Wz matrix')
@click.option('-q', '--quantity', type=int, required=False, default=4000, help='quantity of embeddings to read')
@click.option('-k', '--knn', type=int, required=False, default=10, help='k in kNN, if 0 translate without CSLS')
def translate(x, z, xz, wx, wz, quantity, knn):
    """translate provided words"""
    # TODO fix it for work with XWx and ZWz
    if x and z:
        _x = load_vectors(x, quantity)
        _z = load_vectors(z, quantity)
    elif xz:
        _x, _z = get_validation_data(xz)
    else:
        raise ValueError('x and z or xz pairs are required')
    _wx, _wz = numpy.loadtxt(wx), numpy.loadtxt(wz)
    translate_function(_x, _z, _wx, _wz, knn)


@cli.command()
@click.option('-s', '--size', type=int, required=True, help='number of lines to extract')
@click.option('-p', '--paths', type=click.Path(exists=True), required=True, multiple=True,
              help='your file path, you can add multiple')
def cut_file(size, paths):
    """extract <s> first lines from file \n
    This script extracts s+1 lines from a file and saves it in a new folder in your file directory
    """
    # TODO add output path
    for path in paths:
        extract_lines_from_file(size, path)


@cli.command()
@click.option('-d', '--dictionary', type=click.Path(exists=True), required=True, multiple=True,
              help='path to dictionary with parallel words, you can add multiple')
@click.option('-s', '--source', type=click.Path(exists=True), required=True, help='source embeddings path')
@click.option('-t', '--target', type=click.Path(exists=True), required=True, help='target embeddings path')
@click.option('-o', '--output', type=click.Path(), required=True, help='output embeddings path')
@click.option('-q', '--quantity', type=int, required=False, default=None, help='number of embeddings to create')
def create_embeddings_pairs(dictionary, source, target, output, quantity):
    """find embeddings for words in parallel corpora and create pairs"""
    create_supervised_validation_data(dictionary, source, target, output, quantity)


@cli.command()
@click.option('-q', '--quantity', type=int, required=False, default=1, help='How many times repeat arguments')
def benchmark_args(quantity):
    """Create arguments for benchmarks"""
    generate_arguments_combination(quantity)


if __name__ == '__main__':
    cli()
