from copy import copy
from itertools import combinations
from time import time

from src.utils.math_functions import np_


def local_search_greedy(cost_function, initial_dict_size, initial_solution=None, max_time=60, max_improve=-1,
                        verbose=False, minimize=True):
    start_time = time()
    learning_progress = list()
    sorted_indexes = np_.arange(initial_dict_size)
    candidate = sorted_indexes if initial_solution is None else initial_solution
    # sorted_indexes, candidate = _initial_solution_by_sorting_features(Mx, Mz)

    # threshold for initial solution by sorting features
    sorted_indexes, candidate = sorted_indexes[0:initial_dict_size], candidate[0:initial_dict_size]

    best_candidate_cost = cost_function(candidate)
    progress = True
    improve = 0

    while progress:
        progress = False

        for (a, b) in combinations(sorted_indexes, 2):

            if time() - start_time >= max_time or improve == max_improve:
                progress = False
                break

            neighbour = copy(candidate)
            neighbour[a], neighbour[b] = neighbour[b], neighbour[a]
            neighbour_cost = cost_function(neighbour)

            if (neighbour_cost < best_candidate_cost and minimize is True) or (
                    neighbour_cost > best_candidate_cost and minimize is False):
                if verbose:
                    print(time() - start_time, best_candidate_cost)
                learning_progress.append([time() - start_time, best_candidate_cost])
                candidate = neighbour
                best_candidate_cost = neighbour_cost
                improve += 1
                progress = True

    return sorted_indexes, candidate, learning_progress