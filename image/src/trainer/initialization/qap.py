# import localsolver

import numpy as np_

from src.utils.math_functions import asnumpy


def qap(A, B, max_time=1):
    with localsolver.LocalSolver() as ls:
        A = asnumpy(A)
        B = asnumpy(B)

        n = len(A[0])
        model = ls.model

        p = model.list(n)
        model.constraint(model.eq(model.count(p), n))
        array_B = model.array(model.array(B[i]) for i in range(n))

        obj = model.sum(A[i][j] * model.at(array_B, p[i], p[j]) for j in range(n) for i in range(n))
        model.maximize(obj)

        model.close()
        ls.param.time_limit = max_time
        ls.solve()

        sorted_indexes = np_.arange(n)
        target_indexes = np_.array(p.value)

        return sorted_indexes, target_indexes, dict()
