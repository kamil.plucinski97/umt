import time
import random
from copy import copy

from heapq import nsmallest, nlargest
from random import shuffle

import numpy

from src.utils.math_functions import np_


def one_point_crossover(candidates):
    """Performs one point crossover. Randomly chooses intersection point and takes first candidates genotypes up to
    the intersection point and second candidates genes from intersection point to the end.

    Args:
        candidates (List(Vector)): two candidates

    Returns:
        Vector: new candidate
    """
    parent_first, parent_second = candidates[0].get('value'), candidates[1].get('value')
    cut_point = int(np_.random.randint(1, len(parent_first) - 1))
    first_parent_left_part = parent_first[:cut_point]
    first_parent_right_part = parent_first[cut_point:]
    second_parent_left_part = parent_second[:cut_point]
    second_parent_right_part = parent_second[cut_point:]

    _, _, indexes_to_fill = numpy.intersect1d(first_parent_left_part, second_parent_right_part, return_indices=True)
    values_to_fill = numpy.intersect1d(first_parent_right_part, second_parent_left_part)
    numpy.random.shuffle(values_to_fill)  # intersect sorts values
    indexes_to_fill += cut_point

    child = numpy.concatenate((first_parent_left_part, second_parent_right_part))
    for index, value in zip(indexes_to_fill, values_to_fill):
        child[int(index)] = np_.array(value)

    return {'value': child}


def swap_mutation(candidate):
    """Take two positions on the chromosome and interchange these values.
    Args:
        candidate (Vector): chromosome
    Returns:
        candidate (Vector): chromosome
    """
    candidate_value = candidate.get('value')
    idx_a, idx_b = np_.random.randint(0, len(candidate_value), 2)
    itm_a, itm_b = candidate_value[int(idx_a)], candidate_value[int(idx_b)]
    new_candidate_value = copy(candidate_value)
    new_candidate_value[int(idx_a)] = itm_b
    new_candidate_value[int(idx_b)] = itm_a
    candidate['value'] = new_candidate_value
    return candidate


def random_selection(population, fitness, size, minimize=True, number_of_parents=2):
    """Sorts population by fitness and then selects parents with a probability according to their rank. Better parents have higher probability

    Args:
        population (List(Vector)): list of candidates
        fitness (List(float)): list of fitness values corresponding to the candidates
        size (int): how many parents to sample
        minimize (bool, optional): Defaults to False. Determines if lower or higher is better
        number_of_parents (int, optional): Defaults to 2. Size of couples for crossover

    Returns:
        List(List(Vector)): list of length size containing parent chromosomes
    """
    pop = [candidate.get('value') for candidate in population]
    indexes = [i for i in range(len(pop))]
    parents_indexes = [random.choices(indexes, k=number_of_parents) for _ in range(size)]
    parents = [[{'fit': fitness[i], 'value': pop[i]} for i in indexes] for indexes in parents_indexes]
    return parents


def rank_based_selection(population, fitness, size, minimize=True, number_of_parents=2):
    """Sorts population by fitness and then selects parents with a probability according to their rank. Better parents have higher probability

    Args:
        population (List(Vector)): list of candidates
        fitness (List(float)): list of fitness values corresponding to the candidates
        size (int): how many parents to sample
        minimize (bool, optional): Defaults to False. Determines if lower or higher is better
        number_of_parents (int, optional): Defaults to 2. Size of couples for crossover

    Returns:
        List(List(Vector)): list of length size containing parent chromosomes
    """
    pop = [candidate.get('value') for candidate in population]
    fitness_and_pop = list(zip(fitness, pop))
    fitness_and_pop = sorted(fitness_and_pop, reverse=not minimize, key=lambda x: x[0])
    _, pop = zip(*fitness_and_pop)
    # weights = np_.arange(len(pop), 0, -1)
    # weights = weights / np_.sum(weights)
    weights = [1 for _ in range(len(pop))]
    indexes = [i for i in range(len(pop))]
    parents_indexes = [random.choices(indexes, k=number_of_parents, weights=weights) for i in range(size)]
    parents = [[{'fit': fitness[i], 'value': pop[i]} for i in indexes] for indexes in parents_indexes]
    return parents


def tournament_selection(population, fitness, size, minimize=True, number_of_parents=2, k=3):
    parents = list()
    for i, p in enumerate(population):
        p['fit'] = fitness[i]

    for _ in range(size):
        tournament = random.choices(population, k=k)
        tournament = sorted(tournament, reverse=not minimize, key=lambda t: t.get('fit'))
        parents.append(tournament[:number_of_parents])
    return parents


class Algorithm(object):
    def __init__(self):
        self.max_iterations = np_.inf
        self.max_time = np_.inf
        self.iterations = 0
        self.start_time = None
        self.external_exit = False
        # ToDo: We shouldnt be using those, but instead use the results function_calls property
        self.function_calls = {}

    def start(self):
        self.start_time = time.time()

    def has_finished(self):
        time = self.elapsed_time > self.max_time
        iterations = self.iterations > self.max_iterations
        return time or iterations or self.external_exit

    @property
    def elapsed_time(self):
        return time.time() - self.start_time


class Result(object):
    def __init__(self):
        self.learning_progress = None
        self.solution = None
        self.averaged_progress = []
        self.best_progress = []
        self.optimizer_settings = {}
        self.function_calls = {}


class GeneticAlgorithm(Algorithm):
    """Class for the Genetic Algorithm
    Args:
        generations (int): maximum number of generations
        population_size (int): size of each population
        elitism (int): Default is 0, number of best chromosomes which are allowed to survive each generation
        minimize (bool): if True, lower function value is better
    """

    def __init__(self, population_size=100, elitism=0, minimize=True):
        Algorithm.__init__(self)
        self.population_size = population_size
        self.elitism = elitism
        self.minimize = minimize

        self.selection_size = self.population_size - self.elitism
        self.population = None
        self.function_calls = {'fitness': 0, 'crossover': 0, 'mutation': 0, 'selection': 0}

    def optimize(self, generations=np_.inf, max_time=np_.inf):
        """Performs optimization. The optimization follows three steps:
        - for current population calculate fitness
        - select chromosomes with best fitness values with higher propability as parents
        - use parents for reproduction (crossover and mutation)
        - repeat until max number of generation is reached
        Args:
            generations (int): Max Iterations
            max_time (float): maximum time
        Returns:
            Result
        """
        self.max_iterations = generations
        self.max_time = max_time
        learning_progress = list()

        if np_.isinf(self.max_iterations) and np_.isinf(self.max_time):
            raise TypeError('Expect either max_iterations or max_time to be not inf.')

        if self.population is None:
            self.init_population()

        res = Result()
        res.optimizer_settings = {'population_size': self.population_size,
                                  'elitism': self.elitism, 'generations': self.max_iterations,
                                  'time limit': self.max_time}
        self.start()
        while not self.has_finished():
            # calculate fitness for each candidate in the population
            fitness = [self._fitness(candidate.get('value')) for candidate in self.population]
            res.averaged_progress.append(sum(fitness) / len(fitness))

            if self.minimize:
                print(self.elapsed_time, min(fitness), sum(fitness) / len(fitness))
                learning_progress.append([self.elapsed_time, min(fitness)])
                res.best_progress.append(min(fitness))
            else:
                learning_progress.append([self.elapsed_time, max(fitness)])
                res.best_progress.append(max(fitness))

            # get parents
            parents = self._selection(fitness)

            children = list()
            for p in parents:
                decision = random.randint(0, 1)
                if decision:
                    # print('C', end='')
                    child = self._crossover(p)
                else:
                    child = max(p, key=lambda x: x.get('fit'))
                    while not decision:
                        # print('M', end='')
                        child = self._mutate(child)
                        decision = random.randint(0, 1)
                children.append(child)

            if self.elitism > 0:
                if self.minimize:
                    surviving_elite = nsmallest(self.elitism, list(
                        range(len(self.population))), key=lambda x: fitness[x])
                else:
                    surviving_elite = nlargest(self.elitism, list(
                        range(len(self.population))), key=lambda x: fitness[x])
                surviving_elite = [self.population[i] for i in surviving_elite]
                self.population = surviving_elite + children
            else:
                self.population = children

            self.iterations += 1

        res.learning_progress = learning_progress
        res.solution = min([p.get('value') for p in self.population], key=self.fitness)
        res.function_calls = self.function_calls
        return res

    def init_population(self):
        """Initializes Population
        """
        raise NotImplementedError("Either provide an initial population or an init_population method.")

    def _fitness(self, candidate):
        """internal method for calling the fitness method"""
        self.function_calls['fitness'] += 1
        return self.fitness(candidate)

    def fitness(self, candidate):
        """Returns fitness for a given candidate. A
        Args:
            candidate (Vector): candidate from population
        Returns:
            float: scalar fitness value
        """
        raise NotImplementedError("No fitness method found.")

    def _crossover(self, candidates):
        """Internal method for calling the crossover method"""
        self.function_calls['crossover'] += 1
        return self.crossover(candidates)

    def crossover(self, candidates):
        """Performs Crossover of given candidates (usually two)
        Args:
            candidates (List[Vector]): two or more candidates from population
        Returns:
            Vector: new candidate
        """
        raise NotImplementedError("No crossover method found.")

    def _selection(self, fitness):
        """Internal method for calling the selection method"""
        self.function_calls['selection'] += 1
        return self.selection(fitness)

    def selection(self, fitness):
        """Performs selection.
        Args:
            fitness (List(float))
        Raises:
            List(List(candidate)) - selected as parents
        """
        raise NotImplementedError("No selection method found.")

    def _mutate(self, candidate):
        """Internal method for calling the mutation method"""
        self.function_calls['mutation'] += 1
        return self.mutation(candidate)

    def mutation(self, candidate):
        """Performs mutation of candidate
        Args:
            candidate (Vector): candidate from population
        """
        raise NotImplementedError("No mutation method found.")

    def health(self, candidate):
        """Checks health of candidate and returns a healthy candidate
        Args:
            candidate (Vector): candidate from population
        """
        # TODO: Integrate health into algorithm
        raise NotImplementedError


class GeneticInitialization(GeneticAlgorithm):
    """
    This class requires metapy library in project directory
    repo url: https://github.com/ccssmnn/metapy
    """

    def __init__(self, number_of_variables, cost_function, population=20, elitism=5):
        super().__init__(population_size=population, elitism=elitism, minimize=True)
        self.cost_function = cost_function
        self.population = [self._random_candidate(number_of_variables) for i in range(self.population_size)]

    def fitness(self, candidate):
        return self.cost_function(candidate)

    def crossover(self, candidates):
        return one_point_crossover(candidates)

    def mutation(self, candidate):
        return swap_mutation(candidate)

    def selection(self, fitness):
        return tournament_selection(self.population, fitness, self.selection_size, minimize=self.minimize,
                                    number_of_parents=2, k=4)

    @staticmethod
    def _random_candidate(number_of_variables):
        x = list(range(number_of_variables))
        shuffle(x)
        return {'value': x}


def genetic_algorithm(initial_dict_size, cost_function, max_time=3600):
    ga = GeneticInitialization(initial_dict_size, cost_function)
    res = ga.optimize(max_time=max_time)
    result = np_.array([int(i) for i in res.solution])
    solution = np_.arange(initial_dict_size)
    return solution, result, res.learning_progress
