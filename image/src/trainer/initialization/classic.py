from src.utils.math_functions import knn, np_


def classic_initialization(initial_dict_size, Mx, Mz, k):
    similarity_matrix = Mx.dot(Mz.T)
    # TODO duplicated code with sorting_features.py
    knn_row_mean_similarity = knn(similarity_matrix, k=k)
    knn_column_mean_similarity = knn(similarity_matrix.T, k=k)
    average_knn_similarity_matrix = knn_row_mean_similarity[:, np_.newaxis] / 2 + knn_column_mean_similarity / 2
    similarity_matrix -= average_knn_similarity_matrix

    source_indexes = np_.concatenate((np_.arange(initial_dict_size), similarity_matrix.argmax(axis=0)))
    target_indexes = np_.concatenate((similarity_matrix.argmax(axis=1), np_.arange(initial_dict_size)))
    del similarity_matrix, knn_column_mean_similarity, knn_row_mean_similarity, average_knn_similarity_matrix
    return source_indexes, target_indexes, list()
