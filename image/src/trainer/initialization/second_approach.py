from collections import defaultdict
from random import randint

from src.utils.math_functions import np_, knn, asnumpy


class Mapping(object):
    def __init__(self, *args):
        self.from_index, self.to_index, self.weight = args

    def __str__(self):
        return '{} - {} : {}'.format(self.from_index, self.to_index, self.weight)


def second_approach(Mx, Mz, k=10):
    IMx = Mx.argsort(axis=1).argsort(axis=1)
    IMz = Mz.argsort(axis=1).argsort(axis=1)

    similarity_matrix = Mx.dot(Mz.T)
    # TODO duplicated code with sorting_features.py
    knn_row_mean_similarity = knn(similarity_matrix, k=k)
    knn_column_mean_similarity = knn(similarity_matrix.T, k=k)
    average_knn_similarity_matrix = knn_row_mean_similarity[:, np_.newaxis] / 2 + knn_column_mean_similarity / 2
    similarity_matrix -= average_knn_similarity_matrix

    target_indexes = similarity_matrix.argmax(axis=1)

    mapping_list = list()
    for row_index in range(len(IMx)):
        IMx_row = IMx[row_index]
        IMz_row = IMz[target_indexes[row_index]]

        IMz_row_swapped = [0] * len(IMz_row)
        for index, value in enumerate(asnumpy(IMz_row)):
            IMz_row_swapped[value] = index

        mappings = [Mapping(index, IMz_row_swapped[int(value)], value) for index, value in enumerate(IMx_row)]
        sorted_mappings = sorted(mappings, key=lambda m: m.weight)
        mapping_list.extend(sorted_mappings[-500:])

    permutation_index_count = [defaultdict(int) for _ in range(len(IMx))]
    for mapping in mapping_list:
        permutation_index_count[mapping.from_index][mapping.to_index] += 1

    for permutation_count in permutation_index_count:
        if len(permutation_count) == 0:
            permutation_count[randint(0, len(IMx) - 1)] = 1

    best_permutation = [max(word_dict, key=word_dict.get) for word_dict in permutation_index_count]

    del similarity_matrix, knn_row_mean_similarity, knn_column_mean_similarity, average_knn_similarity_matrix
    del mappings, permutation_index_count, permutation_count,
    return np_.arange(len(best_permutation)), best_permutation, list()
