from src.utils.math_functions import np_


def cost_classic(solution, Mx, Mz):
    solution = [int(i) for i in solution]
    return np_.linalg.norm(Mx - Mz[solution][:, solution], ord='fro')


def cost_percentage_threshold(solution, Mx, Mz, threshold=0.5):
    quantity_threshold = int(len(solution) * threshold)
    return cost_quantity_threshold(solution, Mx, Mz, quantity_threshold)


def cost_quantity_threshold(solution, Mx, Mz, threshold=300):
    solution = [int(i) for i in solution]
    if threshold > len(solution):
        threshold = len(solution)
    to_remove = len(solution) - threshold

    # TODO calculate it only once and pass as argument into function
    id_to_remove = np_.argpartition(Mx, to_remove)[:, :to_remove]
    difference = Mx - Mz[solution][:, solution]
    difference[np_.arange(Mx.shape[0])[:, None], id_to_remove] = 0
    return np_.linalg.norm(difference, ord='fro')
