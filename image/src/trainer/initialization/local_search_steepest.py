from copy import copy
from time import time
from itertools import combinations

from src.utils.math_functions import np_


def local_search_steepest(cost_function, initial_dict_size, max_time=3600, initial_solution=None):
    start_time = time()
    learning_progress = list()

    sorted_indexes = np_.arange(initial_dict_size)
    candidate = sorted_indexes if initial_solution is None else initial_solution
    # sorted_indexes, candidate = _initial_solution_by_sorting_features(Mx, Mz)
    # threshold for initial solution by sorting features
    sorted_indexes, candidate = sorted_indexes[0:initial_dict_size], candidate[0:initial_dict_size]

    best_candidate_cost = cost_function(candidate)
    progress = True

    while progress:
        progress = False

        iteration_leader = candidate
        iteration_leader_cost = best_candidate_cost

        for (a, b) in combinations(sorted_indexes, 2):
            if time() - start_time >= max_time:
                progress = False
                break
            neighbour = copy(candidate)
            neighbour[a], neighbour[b] = neighbour[b], neighbour[a]
            neighbour_cost = cost_function(neighbour)

            if neighbour_cost < iteration_leader_cost:
                iteration_leader = neighbour
                iteration_leader_cost = neighbour_cost

        if iteration_leader_cost < best_candidate_cost:
            print(time() - start_time, best_candidate_cost)
            learning_progress.append([time() - start_time, best_candidate_cost])
            candidate = iteration_leader
            best_candidate_cost = iteration_leader_cost
            progress = True

    return sorted_indexes, candidate, learning_progress
