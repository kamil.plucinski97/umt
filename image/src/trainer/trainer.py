import collections
import re
import time
from collections import OrderedDict
from copy import deepcopy

import numpy
import pandas as pd

from src.trainer.initialization._cost_function import cost_classic, cost_percentage_threshold, cost_quantity_threshold
from src.trainer.initialization.classic import classic_initialization
from src.trainer.initialization.genetic import genetic_algorithm
from src.trainer.initialization.local_search_greedy import local_search_greedy
from src.trainer.initialization.local_search_steepest import local_search_steepest
from src.trainer.initialization.qap import qap
from src.trainer.initialization.second_approach import second_approach
from src.utils.handling_data import create_directory_if_necessary, write
from src.utils.math_functions import construct_squared_similarity_matrix, knn, whitening_transformation, dropout, np_

BATCH_SIZE = 10000
DTYPE = 'float32'


class Trainer:
    def __init__(self, config, X, Z):
        self.config = config
        self._result_config = deepcopy(config._config)
        self._result_config['source'] = self._get_language_from_embedding_path(config.source_embeddings_path)
        self._result_config['target'] = self._get_language_from_embedding_path(config.target_embeddings_path)
        self._result_config['embedding'] = self._get_embedding_from_path(config.target_embeddings_path)

        self.keep_probability = config.keep_probability
        self.X, self.Z = X, Z

        self.last_improvement, self.iteration = 0, 1
        self.objective, self.best_objective = 0, -100
        self.threshold = config.threshold
        if self.threshold is None:
            self.threshold = 0.000001
        self.end = False
        self.source_indexes, self.target_indexes, self.XWx, self.ZWz, self.validation = [None] * 5

        self.name = self._create_unique_name()
        self.learning_progress = list()
        self.initialization_progress = list()
        self.initialization_progress.append(['time', 'cost'])
        self.start_time = None

    def initial_solution(self):
        size = self.config.quantity_words_to_initialize
        normalized_X, Mx, = construct_squared_similarity_matrix(self.X, size)
        normalized_Z, Mz, = construct_squared_similarity_matrix(self.Z, size)
        self.X, self.Z = normalized_X, normalized_Z

        if self.config.cost == 'classic':
            cost_function = lambda solution: cost_classic(solution, np_.copy(Mx), np_.copy(Mz))
        elif self.config.cost == 'percentage':
            cost_function = lambda solution: cost_percentage_threshold(solution, np_.copy(Mx), np_.copy(Mz),
                                                                       self.config.cost_threshold)
        elif self.config.cost == 'quantity':
            cost_function = lambda solution: cost_quantity_threshold(solution, np_.copy(Mx), np_.copy(Mz),
                                                                     self.config.cost_threshold)
        else:
            raise NotImplementedError

        algorithm = self.config.initialization_algorithm

        if algorithm == 'classic':
            self.source_indexes, self.target_indexes, initialization_progress = classic_initialization(
                self.config.quantity_words_to_initialize, Mx, Mz, self.config.k)
        elif algorithm == 'random':
            self.source_indexes = np_.arange(self.config.quantity_words_to_initialize)
            self.target_indexes = np_.arange(self.config.quantity_words_to_initialize)
            np_.random.shuffle(self.target_indexes)
            initialization_progress = {}
        elif algorithm == 'steepest':
            self.source_indexes, self.target_indexes, initialization_progress = local_search_steepest(
                cost_function, self.config.quantity_words_to_initialize, max_time=self.config.initialization_time)
        elif algorithm == 'greedy':
            _, initial_solution, _ = classic_initialization(self.config.quantity_words_to_initialize, Mx, Mz,
                                                            self.config.k)
            initial_solution = initial_solution[:self.config.quantity_words_to_initialize]

            self.source_indexes, self.target_indexes, initialization_progress = local_search_greedy(
                cost_function, self.config.quantity_words_to_initialize, max_time=self.config.initialization_time
            )
        elif algorithm == 'greedy-random':
            self.source_indexes, self.target_indexes, initialization_progress = local_search_greedy(
                cost_function, self.config.quantity_words_to_initialize, max_time=self.config.initialization_time
            )
        elif algorithm == 'genetic':
            self.source_indexes, self.target_indexes, initialization_progress = genetic_algorithm(
                self.config.quantity_words_to_initialize, cost_function, max_time=self.config.initialization_time)
        elif algorithm == 'second':
            self.source_indexes, self.target_indexes, initialization_progress = second_approach(Mx, Mz, self.config.k)
        elif algorithm == 'qap':
            self.source_indexes, self.target_indexes, initialization_progress = qap(Mx, Mz,
                                                                                    max_time=self.config.initialization_time)
        else:
            raise NotImplementedError

        self.initialization_progress.extend(initialization_progress)
        self._result_config['initialization_cost_function'] = cost_function(
            self.target_indexes[:self.config.quantity_words_to_initialize])
        self.start_time = time.time()

        del Mx, Mz

    def train_step(self):

        if not self.end:
            self.XWx, self.ZWz = self._step()
        else:
            self.XWx, self.ZWz = self._final_step()

        if self.end or self.config.only_initialization:
            self._result_config['learning_time'] = time.time() - self.start_time
            return self.objective, True

        best_similarity_forward, new_target_indexes_forward = self._forward_learning()
        best_similarity_backward, new_source_indexes_backward = self._backward_learning()

        # new_source_indexes_backward, new_target_indexes_forward = self._third_step(new_source_indexes_backward, new_target_indexes_forward)

        self.source_indexes = np_.concatenate(
            (np_.arange(len(new_target_indexes_forward)), new_source_indexes_backward))
        self.target_indexes = np_.concatenate(
            (new_target_indexes_forward, np_.arange(len(new_source_indexes_backward))))

        self.objective = float((np_.mean(best_similarity_forward) + np_.mean(best_similarity_backward)).tolist() / 2)

        self._validate_objective_update()
        self.iteration += 1
        self._validate_progress()

        self._result_config['learning_time'] = time.time() - self.start_time
        return self.objective, False

    def validate(self):
        if self.iteration - 1 == 1:
            labels = ['It', 'Val_acc', 'Val_sim', 'Obj', 'No_imp', 'Keep_prob']
            self.learning_progress.append(labels)
            if self.config.verbose:
                print("{:<5}{:<10} {:<10} {:<10} {:<10} {:<10}".format(*labels))

        source_words = list(self.validation.keys())
        translation = collections.defaultdict(int)
        best_sim = list()

        for i in range(0, len(source_words), BATCH_SIZE):
            j = min(i + BATCH_SIZE, len(source_words))
            similarities = self.XWx[source_words[i:j]].dot(self.ZWz.T)
            nn = similarities.argmax(axis=1).tolist()
            for k in range(j - i):
                translation[source_words[i + k]] = nn[k]
                matching_similarities = [float(similarities[k, j]) for j in
                                         self.validation[source_words[i + k]]]  # cupy requires float here
                best_sim.append(max(matching_similarities))

        accuracy = numpy.mean([1 if translation[i] in self.validation[i] else 0 for i in source_words])
        similarity = numpy.mean(best_sim)

        self._save_result_to_config(accuracy, similarity)
        self.learning_progress.append(
            [self.iteration, accuracy, similarity, float(round(self.objective, 1)),
             self.iteration - self.last_improvement])

        if self.config.verbose:
            # float is required, known issue
            # https://github.com/numpy/numpy/issues/5543
            # it works for numpy but not for cupy
            print('{:<5} {:<10.4%} {:<10.2%} {:<10.2%} {:<10} {:<10}'.format(self.iteration - 1, float(accuracy),
                                                                             float(similarity),
                                                                             float(round(self.objective, 5)),
                                                                             self.iteration - self.last_improvement,
                                                                             self.keep_probability))

    def save_output(self):
        paths = ['output', 'output/initializations', 'output/results', 'output/learning']
        for path in paths:
            create_directory_if_necessary(path)

        self._result_config['name'] = self.name

        if len(self.initialization_progress) >= 2:
            self._result_config['initialization_cost_curve'] = True

            df = pd.DataFrame(self.initialization_progress)
            df.to_csv('output/initializations/{}.csv'.format(self.name), index=False)
        else:
            self._result_config['initialization_cost_curve'] = False

        df = pd.DataFrame([self._result_config])
        df.to_csv('output/results/{}.csv'.format(self.name), index=False)

        df = pd.DataFrame(self.learning_progress)
        df.to_csv('output/learning/{}.csv'.format(self.name), index=False)
        return df

    def _step(self):
        similarity = self.Z[self.target_indexes].T.dot(self.X[self.source_indexes])
        U, S, Vt = np_.linalg.svd(similarity)
        Wx = Vt.T.dot(U.T)
        XWx = self.X.dot(Wx)
        ZWz = self.Z
        return XWx, ZWz

    def _third_step(self, source_permutation, target_permutation):
        words_range = np_.arange(self.config.quantity_words_to_learn)

        # FORWARD
        goal_function = lambda permutation: np_.sum(self.Z[permutation].T.dot(self.X[words_range]))
        _, new_target, _ = local_search_greedy(goal_function, self.config.quantity_words_to_learn,
                                               np_.copy(target_permutation),
                                               max_improve=5, minimize=False)

        # BACKWARD
        # cost_function = lambda permutation: np_.sum(self.Z[words_range].T.dot(self.X[permutation]))
        # _, new_source, _ = local_search_greedy(cost_function, len(self.source_indexes), np_.copy(source_permutation),
        #                                        max_improve=5)
        return source_permutation, new_target

    def _final_step(self):
        XWx, ZWz = self.X, self.Z

        # TODO rename
        XWx, ZWz, whitened_matrices = self._whitening_transformation(XWx, ZWz)
        WxO, S, WzO_t = self._orthogonal_mapping(XWx, ZWz)
        WzO = WzO_t.T

        XWx, ZWz = self._re_weighting(XWx, ZWz, S, WxO, WzO)
        XWx, ZWz = self._de_whitening(XWx, ZWz, whitened_matrices, WxO, WzO)

        return XWx, ZWz

    def _whitening_transformation(self, XWx, ZWz):
        whitened_Wx = whitening_transformation(XWx[self.source_indexes])
        whitened_Wz = whitening_transformation(ZWz[self.target_indexes])

        XWx = XWx.dot(whitened_Wx)
        ZWz = ZWz.dot(whitened_Wz)

        return XWx, ZWz, (whitened_Wx, whitened_Wz)

    def _orthogonal_mapping(self, XWx, ZWz):
        return np_.linalg.svd(XWx[self.source_indexes].T.dot(ZWz[self.target_indexes]))

    @staticmethod
    def _re_weighting(XWx, ZWz, S, orthogonal_Wx, orthogonal_Wz):
        XWx = XWx.dot(orthogonal_Wx) * S ** 0.5
        ZWz = ZWz.dot(orthogonal_Wz) * S ** 0.5

        return XWx, ZWz

    @staticmethod
    def _de_whitening(XWx, ZWz, whitened_matrices, orthogonal_Wx, orthogonal_Wz):
        whitened_Wx, whitened_Wz = whitened_matrices
        XWx = XWx.dot(orthogonal_Wx.T.dot(np_.linalg.inv(whitened_Wx)).dot(orthogonal_Wx))
        ZWz = ZWz.dot(orthogonal_Wz.T.dot(np_.linalg.inv(whitened_Wz)).dot(orthogonal_Wz))

        return XWx, ZWz

    def _forward_learning(self):
        source_size = min(self.XWx.shape[0], self.config.quantity_words_to_learn)
        target_size = min(self.ZWz.shape[0], self.config.quantity_words_to_learn)
        similarity_Z_to_X = np_.empty((BATCH_SIZE, source_size), dtype=DTYPE)
        similarity_X_to_Z = np_.empty((BATCH_SIZE, target_size), dtype=DTYPE)
        knn_column_mean_similarity = np_.zeros(target_size, dtype=DTYPE)
        new_target_indexes_forward = np_.zeros(source_size, dtype=int)
        best_similarity_forward = np_.full(source_size, -100, dtype=DTYPE)

        for i in range(0, target_size, similarity_Z_to_X.shape[0]):
            j = min(i + similarity_Z_to_X.shape[0], target_size)
            similarity_Z_to_X[:j - i] = self.ZWz[i:j].dot(self.XWx[:source_size].T)
            knn_column_mean_similarity[i:j] = knn(similarity_Z_to_X[:j - i], k=self.config.k, inplace=True)

        for i in range(0, source_size, similarity_X_to_Z.shape[0]):
            j = min(i + similarity_X_to_Z.shape[0], source_size)
            similarity_X_to_Z[:j - i] = self.XWx[i:j].dot(self.ZWz[:target_size].T)
            best_similarity_forward[i:j] = similarity_X_to_Z[:j - i].max(axis=1)
            similarity_X_to_Z[:j - i] -= knn_column_mean_similarity / 2
            new_target_indexes_forward[i:j] = dropout(similarity_X_to_Z[:j - i],
                                                      1 - self.keep_probability).argmax(axis=1)
        return best_similarity_forward, new_target_indexes_forward

    def _backward_learning(self):
        source_size = min(self.XWx.shape[0], self.config.quantity_words_to_learn)
        target_size = min(self.ZWz.shape[0], self.config.quantity_words_to_learn)
        best_similarity_backward = np_.full(target_size, -100, dtype=DTYPE)
        new_source_indexes_backward = np_.zeros(target_size, dtype=int)
        knn_row_mean_similarity = np_.zeros(source_size, dtype=DTYPE)
        similarity_Z_to_X = np_.empty((BATCH_SIZE, source_size), dtype=DTYPE)
        similarity_X_to_Z = np_.empty((BATCH_SIZE, target_size), dtype=DTYPE)

        for i in range(0, source_size, similarity_X_to_Z.shape[0]):
            j = min(i + similarity_X_to_Z.shape[0], source_size)
            similarity_X_to_Z[:j - i] = self.XWx[i:j].dot(self.ZWz[:target_size].T)
            knn_row_mean_similarity[i:j] = knn(similarity_X_to_Z[:j - i], k=self.config.k, inplace=True)
        for i in range(0, target_size, similarity_Z_to_X.shape[0]):
            j = min(i + similarity_Z_to_X.shape[0], target_size)
            similarity_Z_to_X[:j - i] = self.ZWz[i:j].dot(self.XWx[:source_size].T)
            best_similarity_backward[i:j] = similarity_Z_to_X[:j - i].max(axis=1)
            similarity_Z_to_X[:j - i] -= knn_row_mean_similarity / 2
            new_source_indexes_backward[i:j] = dropout(similarity_Z_to_X[:j - i], 1 - self.keep_probability).argmax(
                axis=1)

        return best_similarity_backward, new_source_indexes_backward

    def _validate_objective_update(self):
        if self.objective - self.best_objective >= self.threshold:
            self.last_improvement = self.iteration
            self.best_objective = self.objective

    def _validate_progress(self):
        if self.iteration - self.last_improvement > self.config.wait_to_objective_improve:
            if self.keep_probability >= 1.0:
                self.end = True
            self.keep_probability = min(1.0, 2 * self.keep_probability)
            self.last_improvement = self.iteration

    # TODO Trainer should not be responsible for it?
    def _create_unique_name(self):
        items = list(self._result_config.items())[3:]
        config = OrderedDict(sorted(items))
        rgx = re.compile('[%s]' % '/.-')
        key_value_names = list()
        for key, value in list(config.items()):
            clean_value = rgx.sub('', str(value))[:3]
            sub_name = key[0] + clean_value
            if key != 'output':
                key_value_names.append(sub_name)

        languages = '{}_{}_'.format(self._result_config['source'], self._result_config['target'])

        connected_sub_names = '_'.join(key_value_names)
        name = '{}_{}'.format(connected_sub_names, self.ts[-4:])
        full_name = languages + name

        return full_name

    @property
    def ts(self):
        return str(time.time()).replace('.', '')

    def _save_result_to_config(self, accuracy, similarity):
        if self.iteration == 1:
            self._result_config['initialization_accuracy'] = accuracy
            self._result_config['initialization_similarity'] = similarity
            self._result_config['initialization_objective'] = self.objective
        else:
            self._result_config['final_accuracy'] = accuracy
            self._result_config['final_similarity'] = similarity
            self._result_config['final_objective'] = self.objective
            self._result_config['learning_time'] = time.time() - self.start_time

    @staticmethod
    def _get_embedding_from_path(path):
        return path.split('/')[-3]

    @staticmethod
    def _get_language_from_embedding_path(path):
        return path.split('/')[-1].split('.')[0]

    @staticmethod
    def create_validation_dict(dict_path, source_word_to_index_dict, target_word_to_index_dict, reverse):
        with open(dict_path) as file:
            validation = collections.defaultdict(set)
            for line in file:
                source_word, target_word = line.split()
                if reverse:
                    source_word, target_word = target_word, source_word
                try:
                    src_ind = source_word_to_index_dict[source_word]
                    trg_ind = target_word_to_index_dict[target_word]
                    validation[src_ind].add(trg_ind)
                except KeyError:
                    pass
        return validation

    def save_results(self, words, quality=None):
        source_words, target_words = words

        source, target = self._result_config['source'], self._result_config['target']
        acc = ''
        if quality is None:
            if self._result_config.get('final_accuracy'):
                acc = str(self._result_config['final_accuracy']).replace('.', '-')
        else:
            acc = quality
        create_directory_if_necessary('output')
        create_directory_if_necessary('output/embeddings')
        result_name = 'output/embeddings/{}_{}-{}_{:.2f}.vec'
        source_path = result_name.format('XWX', source, target, acc)
        target_path = result_name.format('ZWZ', source, target, acc)

        with open(source_path, mode='w', encoding='utf-8', errors='surrogateescape') as file:
            write(source_words, self.XWx, file)

        with open(target_path, mode='w', encoding='utf-8', errors='surrogateescape') as file:
            write(target_words, self.ZWz, file)
