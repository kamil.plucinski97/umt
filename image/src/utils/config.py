class Config:

    def __init__(self, **kwargs):
        self._config = kwargs

    @property
    def source_embeddings_path(self):
        return self._config.get('source')

    @property
    def target_embeddings_path(self):
        return self._config.get('target')

    @property
    def embedding_size(self):
        return self._config.get('embedding_size')

    @property
    def validation_dict_path(self):
        return self._config.get('validation')

    @property
    def reverse_validation(self):
        return self._config.get('reverse')

    @property
    def quantity_words_to_initialize(self):
        return self._config.get('words_initialize')

    @property
    def quantity_words_to_learn(self):
        return self._config.get('words_learn')

    @property
    def wait_to_objective_improve(self):
        return self._config.get('dropout_wait')

    @property
    def k(self):
        return self._config.get('knn')

    @property
    def keep_probability(self):
        return self._config.get('keep_probability')

    @property
    def output_directory(self):
        return self._config.get('output')

    @property
    def only_initialization(self):
        return self._config.get('init_only')

    @property
    def initialization_algorithm(self):
        return self._config.get('init')

    @property
    def initialization_time(self):
        return self._config.get('init_time')

    @property
    def cost(self):
        return self._config.get('cost')

    @property
    def threshold(self):
        return self._config.get('threshold')

    @property
    def save_embeddings(self):
        return self._config.get('save_embeddings')

    @property
    def save_output(self):
        return self._config.get('save_output')

    @property
    def cost_threshold(self):
        result = self._config.get('cost_threshold')
        if self.cost == 'percentage':
            return float(result) / 100
        elif self.cost == 'quantity':
            return int(result)

    @property
    def verbose(self):
        return self._config.get('verbose')

    def __str__(self):
        return str(self._config)
