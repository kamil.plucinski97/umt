from copy import deepcopy
import numpy


def _np():
    try:
        return numpy
        # import cupy
        # print("Use CuPy")
        # return cupy
    except ImportError:
        # print("Use NumPy")
        return numpy


np_ = _np()


def asnumpy(x):
    try:
        import cupy
        return cupy.asnumpy(x)
    except ImportError:
        return numpy.asarray(x)


def construct_squared_similarity_matrix(matrix, size):
    first_normalization = normalize_matrix(matrix)
    U, S, _ = np_.linalg.svd(first_normalization[:size], full_matrices=False)
    second_normalization = (U * S).dot(U.T)
    second_normalization = np_.sort(second_normalization, axis=1)
    second_normalization = normalize_matrix(second_normalization)
    return first_normalization, second_normalization


def knn(m, k, inplace=False):
    if not inplace:
        m = np_.array(m)
    n = m.shape[0]
    result = np_.zeros(n, dtype=m.dtype)
    rows = np_.arange(n)

    if k <= 0:
        return result

    minimum = m.min()
    for i in range(k):
        column = m.argmax(axis=1)
        result += m[rows, column]
        m[rows, column] = minimum

    if not inplace:
        del m

    return result / k


def whitening_transformation(m):
    U, S, V_t = np_.linalg.svd(m, full_matrices=False)
    return V_t.T.dot(np_.diag(1 / S)).dot(V_t)


def dropout(matrix, p):
    mask = np_.random.rand(*matrix.shape) >= p
    return matrix * mask


def length_normalize(matrix):
    norms = np_.sqrt(np_.sum(matrix ** 2, axis=1))
    norms[norms == 0] = 1
    result = matrix / norms[:, np_.newaxis]
    return result


def normalize_matrix(matrix):
    result = length_normalize(matrix)
    avg = np_.mean(result, axis=0)
    result -= avg
    result = length_normalize(result)
    return result
