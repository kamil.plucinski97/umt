import collections
import io
import os
from typing import Dict

import numpy

from src.utils.math_functions import asnumpy


def print_splitting_token():
    print('SPLIT')


def get_validation_data(validation_dict_path, reverse=False):
    fin = io.open(validation_dict_path, 'r', encoding='utf-8', newline='\n', errors='ignore')
    X, Z = dict(), dict()
    for line in fin:
        source, target = line.split(';')
        tokens = source.rstrip().split(' ')
        source_word, source_embedding = tokens[0], list(tokens[1:])
        tokens = target.rstrip().split(' ')
        target_word, target_embedding = tokens[0], list(tokens[1:])
        X[source_word] = list(map(float, source_embedding))
        Z[source_word] = list(map(float, target_embedding))

    if reverse:
        X, Z = Z, X

    return X, Z


def load_vectors(file_name, quantity) -> Dict:
    fin = io.open(file_name, 'r', encoding='utf-8', newline='\n', errors='ignore')
    embeddings_quantity, embedding_length = map(int, fin.readline().split())
    data = collections.OrderedDict()
    for i, line in enumerate(fin):
        if i >= quantity:
            break
        tokens = line.rstrip().split(' ')
        data[tokens[0]] = list(map(float, tokens[1:]))

    if i + 1 < quantity:
        # print("Too small embeddings dict! We expected {} but got {} embeddings".format(quantity, i + 1))
        pass

    return data


def save_matrix(output_file_path, matrix_to_save):
    with open(output_file_path, 'wb') as file:
        numpy_matrix_to_save = asnumpy(matrix_to_save)
        numpy.savetxt(file, numpy_matrix_to_save, fmt='%.6f')


def write(words, matrix, file):
    m = asnumpy(matrix)
    print('%d %d' % m.shape, file=file)
    for i in range(len(words)):
        print(words[i] + ' ' + ' '.join(['%.6g' % x for x in m[i]]), file=file)


def create_directory_if_necessary(path):
    if not os.path.isdir(path):
        os.mkdir(path)
