import collections
import os
from collections import defaultdict

import numpy

from src.trainer.trainer import Trainer
from src.utils.handling_data import load_vectors, create_directory_if_necessary
from src.utils.math_functions import normalize_matrix, np_, knn, length_normalize

source_path = os.getcwd()
source = 'en'
target = 'pl'
x = source_path + '/output/embeddings/XWX_en-pl_31-58.vec'
z = source_path + '/output/embeddings/ZWZ_en-pl_31-58.vec'
VALIDATION_PATH = source_path + '/data/dictionaries/en-pl.test.txt'
reverse = True
x, z = z, x

quantity = 200000
# for quantity in range(5000, 100000, 5000):
k = 10
xwx = load_vectors(x, quantity)
zwz = load_vectors(z, quantity)

XWx = length_normalize(np_.array(list(xwx.values())))
ZWz = length_normalize(np_.array(list(zwz.values())))
source_keys = list(xwx.keys())
target_keys = list(zwz.keys())
source_word_to_index_dict = {word: i for i, word in enumerate(source_keys)}
target_word_to_index_dict = {word: i for i, word in enumerate(target_keys)}
# if quantity == 5000:
validation_dict = Trainer.create_validation_dict(VALIDATION_PATH, source_word_to_index_dict,
                                                 target_word_to_index_dict, reverse)
source_words = list(validation_dict.keys())

knn_row_mean_similarity = np_.zeros(ZWz.shape[0])
knn_column_mean_similarity = np_.zeros(ZWz.shape[0])
translation = collections.defaultdict(int)
certainty = list()
BATCH_SIZE = 500
for source_index in range(0, ZWz.shape[0], BATCH_SIZE):
    j = min(source_index + BATCH_SIZE, ZWz.shape[0])
    knn_row_mean_similarity[source_index:j] = knn(ZWz[source_index:j].dot(XWx.T), k=k, inplace=True)
    # knn_column_mean_similarity[source_index:j] = knn(XWx[source_index:j].dot(ZWz.T), k=k, inplace=True)
for source_index in range(0, ZWz.shape[0], BATCH_SIZE):
    j = min(source_index + BATCH_SIZE, ZWz.shape[0])
    similarities = 2 * XWx[source_index:j].dot(ZWz.T) - knn_row_mean_similarity
    nn = similarities.argmax(axis=1).tolist()
    certainty.extend(similarities.max(axis=1).tolist())
    for k in range(j - source_index):
        translation[source_index + k] = nn[k]

accuracy = numpy.mean(
    [1 if translation[src_ind] in validation_dict[src_ind] else 0 for src_ind in list(validation_dict.keys())])
print('{}'.format(accuracy))

translated_words = list()
min_c = min(certainty)
max_c = max(certainty)
for source_index, target_index in enumerate(translation.values()):
    c = (certainty[source_index] - min_c) / (max_c - min_c)
    translated_words.append(
        '{};{};{:.4f}'.format(source_keys[source_index], target_keys[target_index], c))

result = '\n'.join(translated_words)
t = './output/translations/'
create_directory_if_necessary(t)
with open(t + '{}-{}.txt'.format(source, target), "w") as text_file:
    text_file.write(result)
