import matplotlib.pyplot as plt
import seaborn as sns


def similarity_matrix_chart(Mx, Mz, X, Z):
    """
    This function requires at least 2000 words in each variable and also english as source and polish as target language
    """
    pies = Mz[Z.index.get_loc('pies')]
    dog = Mx[X.index.get_loc('dog')]

    my = Mz[Z.index.get_loc('my')]
    we = Mx[X.index.get_loc('we')]

    dom = Mz[Z.index.get_loc('dom')]
    details = Mx[X.index.get_loc('details')]

    poniewaz = Mz[Z.index.get_loc('ponieważ')]
    games = Mx[X.index.get_loc('games')]

    sns.set_style('darkgrid')

    fig, axes = plt.subplots(2, 2, figsize=(10, 7))

    plot_range = (-0.2, 0.2)
    sns.distplot(pies, hist=False, label='Pies', kde_kws={'clip': plot_range}, ax=axes[0, 0])
    sns.distplot(dog, hist=False, label='Dog', kde_kws={'clip': plot_range}, ax=axes[0, 0])

    sns.distplot(my, hist=False, label='My', kde_kws={'clip': plot_range}, ax=axes[0, 1])
    sns.distplot(we, hist=False, label='We', kde_kws={'clip': plot_range}, ax=axes[0, 1])

    sns.distplot(dom, hist=False, label='Dom', kde_kws={'clip': plot_range}, ax=axes[1, 0])
    sns.distplot(details, hist=False, label='Details', kde_kws={'clip': plot_range}, ax=axes[1, 0])

    sns.distplot(poniewaz, hist=False, label='Ponieważ', kde_kws={'clip': plot_range}, ax=axes[1, 1])
    sns.distplot(games, hist=False, label='Games', kde_kws={'clip': plot_range}, ax=axes[1, 1])

    plt.show()
