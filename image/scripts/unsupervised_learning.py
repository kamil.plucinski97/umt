import os

from sklearn.decomposition import PCA

from src.utils.handling_data import load_vectors
from src.utils.math_functions import np_
from src.trainer.trainer import Trainer, DTYPE


def unsupervised_learning(config):
    # np_.random.seed(0)
    # user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
    # print(user_paths)
    # print('Started with: {}'.format(config))

    source_dict = load_vectors(config.source_embeddings_path, config.embedding_size)
    target_dict = load_vectors(config.target_embeddings_path, config.embedding_size)

    source_dict_values = [value for _, value in source_dict.items()]
    target_dict_values = [value for _, value in target_dict.items()]

    source_dict_keys = list(source_dict.keys())
    target_dict_keys = list(target_dict.keys())

    source_word_to_index_dict = {word: i for i, word in enumerate(source_dict_keys)}
    target_word_to_index_dict = {word: i for i, word in enumerate(target_dict_keys)}

    X = np_.array(source_dict_values[0:config.embedding_size], dtype=DTYPE)
    Z = np_.array(target_dict_values[0:config.embedding_size], dtype=DTYPE)

    trainer = Trainer(config, X, Z)
    trainer.initial_solution()

    if config.validation_dict_path is not None:
        trainer.validation = trainer.create_validation_dict(config.validation_dict_path, source_word_to_index_dict,
                                                            target_word_to_index_dict, config.reverse_validation)

    while True:
        obj, end = trainer.train_step()

        if config.validation_dict_path is not None:
            trainer.validate()

        if end:
            if config.save_output:
                trainer.save_output()
            if config.save_embeddings:
                trainer.save_results((source_dict_keys, target_dict_keys))
            break

    return trainer, (source_dict_keys, target_dict_keys)
