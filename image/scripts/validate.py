import collections

import numpy

from src.trainer.trainer import Trainer, DTYPE
from src.utils.math_functions import np_, knn, length_normalize

BATCH_SIZE = 500


def validation(source_dict, target_dict, validation_dict_path, reverse, k=10):
    XWx = np_.array([value for _, value in source_dict.items()], dtype=DTYPE)
    ZWz = np_.array([value for _, value in target_dict.items()], dtype=DTYPE)

    XWx = length_normalize(XWx)
    ZWz = length_normalize(ZWz)

    source_dict_keys = list(source_dict.keys())
    target_dict_keys = list(target_dict.keys())

    source_word_to_index_dict = {word: i for i, word in enumerate(source_dict_keys)}
    target_word_to_index_dict = {word: i for i, word in enumerate(target_dict_keys)}

    validation_dict = Trainer.create_validation_dict(validation_dict_path, source_word_to_index_dict,
                                                     target_word_to_index_dict, reverse)
    source_words = list(validation_dict.keys())

    translation = collections.defaultdict(int)
    knn_column_mean_similarity = np_.zeros(ZWz.shape[0])
    for i in range(0, ZWz.shape[0], BATCH_SIZE):
        j = min(i + BATCH_SIZE, ZWz.shape[0])
        knn_column_mean_similarity[i:j] = knn(ZWz[i:j].dot(XWx.T), k=k, inplace=True)
    for i in range(0, len(source_words), BATCH_SIZE):
        j = min(i + BATCH_SIZE, len(source_words))
        similarities = 2 * XWx[source_words[i:j]].dot(ZWz.T) - knn_column_mean_similarity
        nn = similarities.argmax(axis=1).tolist()
        for k in range(j - i):
            translation[source_words[i + k]] = nn[k]

    accuracy = numpy.mean([1 if translation[i] in validation_dict[i] else 0 for i in source_words])
    # print('{:.2%}'.format(accuracy))
    return accuracy
