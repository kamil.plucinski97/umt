import numpy as np

from src.utils.math_functions import np_, normalize_matrix, length_normalize


def knn(m, k, inplace=False):
    if not inplace:
        m = np_.array(m)
    n = m.shape[0]
    result = np_.zeros(n, dtype=m.dtype)
    rows = np_.arange(n)

    if k <= 0:
        return result

    minimum = m.min()
    for i in range(k):
        column = m.argmax(axis=1)
        result += m[rows, column]
        m[rows, column] = minimum

    if not inplace:
        del m

    return result / k


np.random.seed(3)
x = np.round(np.random.uniform(low=0, high=1, size=(5, 3)), 1)
z = np.round(np.random.uniform(low=0, high=1, size=(5, 3)), 1)
x = length_normalize(x)
z = length_normalize(z)

sim = x.dot(z.T)

knn_f = knn(sim, k=3)
knn_b = knn(sim.T, k=3)

print(np.argmax(2*sim-knn_b-knn_f, axis=1))