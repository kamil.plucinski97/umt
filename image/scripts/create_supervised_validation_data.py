import collections


def read_embeddings_with_filtering_by_list(path, keywords_list):
    fin = open(path, 'r', encoding='utf-8', errors='surrogateescape', newline='\n')
    embeddings_quantity, embedding_length = map(int, fin.readline().split())

    filtered_embeddings_dict = dict()
    for i, line in enumerate(fin):
        tokens = line.rstrip().split(' ')
        word, embedding = tokens[0], list(tokens[1:embedding_length + 1])
        if word in keywords_list:
            filtered_embeddings_dict[word] = embedding

        if len(keywords_list) == len(filtered_embeddings_dict):
            print("Every embedding was found")
            break

        if (i + 1) % 50000 == 0:
            print('Reading progress: {:0.2f}%'.format(i / embeddings_quantity * 100))

    return filtered_embeddings_dict


def create_supervised_validation_data(pairs_paths, source_embeddings_path, target_embeddings_path, output_file_path,
                                      quantity=None):
    source_targets_pairs = collections.defaultdict(set)

    for path in pairs_paths:
        fin = open(path, 'r', encoding='utf-8', errors='surrogateescape', newline='\n')
        for line in fin:
            source_word, target_word = line.split()
            source_targets_pairs[source_word].add(target_word)

    source_words = list(source_targets_pairs.keys())
    target_words = [word for words_set in source_targets_pairs.values() for word in words_set]

    print('Looking for source embeddings')
    source_embeddings_dict = read_embeddings_with_filtering_by_list(source_embeddings_path, source_words)

    print('Looking for target embeddings')
    target_embeddings_dict = read_embeddings_with_filtering_by_list(target_embeddings_path, target_words)

    LINE_PATTERN = '{} {};{} {}\n'
    size, end = 0, False
    with open(output_file_path, 'w') as output_file:
        for source, targets in source_targets_pairs.items():
            for target in targets:
                if source in source_embeddings_dict and target in target_embeddings_dict:
                    line = LINE_PATTERN.format(
                        source,
                        ' '.join(source_embeddings_dict[source]),
                        target,
                        ' '.join(target_embeddings_dict[target])
                    )
                    output_file.write(line)

                    # TODO maybe we can do this a bit nicer
                    if quantity is not None and size >= quantity:
                        end = True
                        break
                    else:
                        size += 1
            if end:
                break
