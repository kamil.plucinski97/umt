import io

from src.utils.handling_data import create_directory_if_necessary


def extract_lines_from_file(size, path):
    fin = io.open(path, 'r', encoding='utf-8', newline='\n', errors='ignore')
    output = ''
    for i, line in enumerate(fin):
        output += line
        if i >= size:
            break

    main_path, extension = path.rsplit('.', 1)
    directory, file_name = main_path.rsplit('/', 1)
    new_directory = '{}/{}'.format(directory, size)
    new_path = '{}/{}.{}'.format(new_directory, file_name, extension)
    
    print('Save to {}'.format(new_path))
    create_directory_if_necessary(new_directory)

    with open(new_path, 'w') as output_file:
        output_file.write(output)
