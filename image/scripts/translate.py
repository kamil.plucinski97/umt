from src.utils.math_functions import normalize_matrix, np_, knn


def translate_function(X_validation_dict, Z_validation_dict, wx, wz, k=0):
    # TODO refactor
    x = normalize_matrix(np_.array(list(X_validation_dict.values())))
    z = normalize_matrix(np_.array(list(Z_validation_dict.values())))

    wx, wz = np_.array(wx), np_.array(wz)

    XWx = x.dot(wx)
    ZWz = z.dot(wz)

    similarity_matrix = XWx.dot(ZWz.T)

    if k > 0:
        knn_row_mean_similarity = knn(similarity_matrix, k=k)
        similarity_matrix = 2 * similarity_matrix - knn_row_mean_similarity

    target = similarity_matrix.argmax(axis=1)

    source_keys = list(X_validation_dict.keys())
    target_keys = list(Z_validation_dict.keys())

    for source_index, target_index in enumerate(target):
        print('{} - {}'.format(source_keys[source_index], target_keys[target_index]))
