import itertools
from copy import deepcopy


def generate_arguments_combination(quantity):
    # languages = [['en', 'es'], ['en', 'de'], ['en', 'fi'], ['pl', 'en']]
    languages = [['en', 'es'], ['en', 'fi']]
    # languages = [['en', 'es']]
    emb = ['word2vec']
    path = 'data/embeddings/{}/30000/{}.emb.vec'
    v_path = 'data/dictionaries/{}-{}.train.txt'
    commands = list()
    for e in emb:
        for s, t in languages:
            if s == 'en':
                line = '-s {} -t {} -v {}'.format(
                    path.format(e, s),
                    path.format(e, t),
                    v_path.format(s, t)
                )
            else:
                line = '-s {} -t {} -v {} --reverse'.format(
                    path.format(e, s),
                    path.format(e, t),
                    v_path.format(t, s)
                )
            commands.append(line)

    init_sets = ['-wi 2000']
    # initializations = ['-i classic', '-i greedy -it 3600 -c classic', '-i greedy -it 3600 -c quantity -ct 10']
    initializations = ['-i greedy-random -it 3600 -c classic', '-i greedy -it 3600 -c classic']

    final_result = list()

    for j in range(quantity):
        result = [' '.join(i) for i in itertools.product(commands, init_sets, initializations)]
        final_result.extend(deepcopy(result))

    print('Created {} arguments'.format(len(final_result)))
    with open('args_list.txt', 'w') as f:
        for item in final_result:
            f.write("%s\n" % item)
