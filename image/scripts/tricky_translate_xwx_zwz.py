import os
from collections import defaultdict

from src.trainer.trainer import Trainer
from src.utils.handling_data import load_vectors
from src.utils.math_functions import normalize_matrix, np_, knn, length_normalize

source_path = os.getcwd()
x = source_path+'/../translate_output/output/embeddings/XWX_en-pl_0-30244104261481175.vec'
z = source_path+'/../translate_output/output/embeddings/ZWZ_en-pl_0-30244104261481175.vec'

# x = source_path+'/output/embeddings/XWX_en-pl_31-58.vec'
# z = source_path+'/output/embeddings/ZWZ_en-pl_31-58.vec'
VALIDATION_PATH = source_path + '/../data/dictionaries/en-pl.test.txt'

quantity = 2000
k = 10
xwx = load_vectors(x, quantity)
zwz = load_vectors(z, quantity)

XWx = length_normalize(np_.array(list(xwx.values())))
ZWz = length_normalize(np_.array(list(zwz.values())))
source_keys = list(xwx.keys())
target_keys = list(zwz.keys())
source_word_to_index_dict = {word: i for i, word in enumerate(source_keys)}
target_word_to_index_dict = {word: i for i, word in enumerate(target_keys)}
validation_dict = Trainer.create_validation_dict(VALIDATION_PATH, source_word_to_index_dict,
                                                     target_word_to_index_dict, False)
BATCH_SIZE = 500
target_x_to_z = defaultdict()
knn_row_mean_similarity = np_.zeros(ZWz.shape[0])
best_similarities_list = list()

for i in range(0, ZWz.shape[0], BATCH_SIZE):
    j = min(i + BATCH_SIZE, ZWz.shape[0])
    knn_row_mean_similarity[i:j] = knn(ZWz[i:j].dot(XWx.T), k=k, inplace=True)
for i in range(0, ZWz.shape[0], BATCH_SIZE):
    j = min(i + BATCH_SIZE, ZWz.shape[0])
    similarities = 2 * XWx[i:j].dot(ZWz.T) - knn_row_mean_similarity
    nn = similarities.argmax(axis=1).tolist()
    best_similarities_list.extend(similarities.max(axis=1).tolist())
    for k in range(j - i):
        target_x_to_z[i + k] = nn[k]

ZWz, XWx = XWx, ZWz
target_z_to_x = defaultdict(int)
for i in range(0, ZWz.shape[0], BATCH_SIZE):
    j = min(i + BATCH_SIZE, ZWz.shape[0])
    knn_row_mean_similarity[i:j] = knn(ZWz[i:j].dot(XWx.T), k=k, inplace=True)
for i in range(0, ZWz.shape[0], BATCH_SIZE):
    j = min(i + BATCH_SIZE, ZWz.shape[0])
    similarities = 2 * XWx[i:j].dot(ZWz.T) - knn_row_mean_similarity
    nn = similarities.argmax(axis=1).tolist()
    best_similarities_list.extend(similarities.max(axis=1).tolist())
    for k in range(j - i):
        target_z_to_x[i + k] = nn[k]
it = 0
for s_i, t_i in target_x_to_z.items():
    if target_z_to_x[t_i] == s_i:
        it+=1
print(it, round(it/len(target_z_to_x)*100,2),len(validation_dict),2)
accuracy = np_.mean(
    np_.array(
        [1 if target_x_to_z[src_ind] in validation_dict[src_ind] else 0 for src_ind in list(validation_dict.keys())]))
print('{} {} {} {}'.format(quantity, accuracy, len(validation_dict), round(len(validation_dict) / 4999 * 100)))
