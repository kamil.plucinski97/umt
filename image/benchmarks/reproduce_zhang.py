import collections
import os

import numpy

from scripts.unsupervised_learning import unsupervised_learning
from scripts.validate import BATCH_SIZE
from src.utils.config import Config
from src.utils.math_functions import length_normalize, np_, knn

OUTPUT_FILE_NAME = 'output/tables_and_plots/table1-reproduce_zhang.txt'
args = {
    'source': '.',
    'target': '.',
    'validation': None,
    'reverse': False,
    'embedding_size': 200000,
    'words_initialize': 4000,
    'words_learn': 20000,
    'dropout_wait': 50,
    'knn': 10,
    'keep_probability': 0.1,
    'output': './',
    'init_only': False,
    'init': 'classic',
    'init_time': 3600,
    'cost': 'classic',
    'cost_threshold': 100.0,
    'save_embeddings': False,
    'save_output': False,
    'verbose': True,
}
proj_dir = os.getcwd()
path = proj_dir + '/data/embeddings/{}/200000/{}.emb.vec'
v_path = proj_dir + '/data/dictionaries/{}-{}.test.txt'

emb = 'word2vec'
for source, target, valid_path, reverse in [
    ['data/zhang/es-en/word2vec.es', 'data/zhang/es-en/word2vec.en', 'data/dictionaries/en-es.train.txt', False],
    ['data/zhang/it-en/word2vec.it', 'data/zhang/it-en/word2vec.en', 'data/dictionaries/en-it.train.txt', False],
    ['data/zhang/tr-en/word2vec.tr', 'data/zhang/tr-en/word2vec.en', 'data/dictionaries/en-tr.train.txt', False],
]:

    with open(OUTPUT_FILE_NAME, 'a+') as file:
        print(source, file=file)
    source, target = target, source
    for _ in range(10):
        args['source'] = source
        args['target'] = target
        args['validation'] = valid_path
        args['reverse'] = reverse

        config = Config(**args)
        trainer, keys = unsupervised_learning(config)

        XWx = length_normalize(trainer.XWx)
        ZWz = length_normalize(trainer.ZWz)

        source_dict_keys, target_dict_keys = keys

        source_word_to_index_dict = {word: i for i, word in enumerate(source_dict_keys)}
        target_word_to_index_dict = {word: i for i, word in enumerate(target_dict_keys)}

        validation_dict = trainer.create_validation_dict(config.validation_dict_path, source_word_to_index_dict,
                                                         target_word_to_index_dict, config.reverse_validation)
        source_words = list(validation_dict.keys())

        translation = collections.defaultdict(int)
        knn_column_mean_similarity = np_.zeros(ZWz.shape[0])
        for i in range(0, ZWz.shape[0], BATCH_SIZE):
            j = min(i + BATCH_SIZE, ZWz.shape[0])
            knn_column_mean_similarity[i:j] = knn(ZWz[i:j].dot(XWx.T), k=config.k, inplace=True)
        for i in range(0, len(source_words), BATCH_SIZE):
            j = min(i + BATCH_SIZE, len(source_words))
            similarities = 2 * XWx[source_words[i:j]].dot(ZWz.T) - knn_column_mean_similarity
            nn = similarities.argmax(axis=1).tolist()
            for k in range(j - i):
                translation[source_words[i + k]] = nn[k]

        accuracy = numpy.mean([1 if translation[i] in validation_dict[i] else 0 for i in source_words])

        with open(OUTPUT_FILE_NAME, 'a+') as file:
            print('{} {}'.format(accuracy, trainer._result_config['learning_time']), file=file)

for source, target, valid_path, reverse in [
    ['data/zhang/es-en/word2vec.es', 'data/zhang/es-en/word2vec.en', 'data/dictionaries/en-es.train.txt', True],
    ['data/zhang/it-en/word2vec.it', 'data/zhang/it-en/word2vec.en', 'data/dictionaries/en-it.train.txt', True],
    ['data/zhang/tr-en/word2vec.tr', 'data/zhang/tr-en/word2vec.en', 'data/dictionaries/en-tr.train.txt', True],
]:

    with open(OUTPUT_FILE_NAME, 'a+') as file:
        print(source, file=file)
    source, target = target, source
    for _ in range(10):
        args['source'] = target
        args['target'] = source
        args['validation'] = valid_path
        args['reverse'] = reverse

        config = Config(**args)
        trainer, keys = unsupervised_learning(config)

        XWx = length_normalize(trainer.XWx)
        ZWz = length_normalize(trainer.ZWz)

        source_dict_keys, target_dict_keys = keys

        source_word_to_index_dict = {word: i for i, word in enumerate(source_dict_keys)}
        target_word_to_index_dict = {word: i for i, word in enumerate(target_dict_keys)}

        validation_dict = trainer.create_validation_dict(config.validation_dict_path, source_word_to_index_dict,
                                                         target_word_to_index_dict, config.reverse_validation)
        source_words = list(validation_dict.keys())

        translation = collections.defaultdict(int)
        knn_column_mean_similarity = np_.zeros(ZWz.shape[0])
        for i in range(0, ZWz.shape[0], BATCH_SIZE):
            j = min(i + BATCH_SIZE, ZWz.shape[0])
            knn_column_mean_similarity[i:j] = knn(ZWz[i:j].dot(XWx.T), k=config.k, inplace=True)
        for i in range(0, len(source_words), BATCH_SIZE):
            j = min(i + BATCH_SIZE, len(source_words))
            similarities = 2 * XWx[source_words[i:j]].dot(ZWz.T) - knn_column_mean_similarity
            nn = similarities.argmax(axis=1).tolist()
            for k in range(j - i):
                translation[source_words[i + k]] = nn[k]

        accuracy = numpy.mean([1 if translation[i] in validation_dict[i] else 0 for i in source_words])

        with open(OUTPUT_FILE_NAME, 'a+') as file:
            print('{} {}'.format(accuracy, trainer._result_config['learning_time']), file=file)
