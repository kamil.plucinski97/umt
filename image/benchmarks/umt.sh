#!/bin/bash

cat args_list.txt | while read -r line; do
  python3 -u umt.py train -wl 20000 -k 10 -dw 50 -kp 0.1 $line
done
