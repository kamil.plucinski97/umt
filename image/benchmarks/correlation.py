from scipy.stats import pearsonr, kendalltau
from src.utils.handling_data import load_vectors
import numpy as np
import os

OUTPUT_FILE_NAME = 'output/tables_and_plots/correlation.txt'


def calc_cover(k1, k2):
    common_part = set(k1) & set(k2)
    keys1 = [key for key in k1 if key in common_part]
    keys2 = [key for key in k2 if key in common_part]
    x_vec = np.arange(len(common_part))
    y_vec = [keys2.index(key) for key in keys1]

    pearson = pearsonr(x_vec, y_vec)
    with open(OUTPUT_FILE_NAME, 'a+') as file:
        print('Cover: {:.2%}, Spearman: {}\n'.format(len(common_part) / len(k1), pearson), file=file)


quant = 20000

for ln in ['en', 'de', 'es', 'it', 'pl', 'fi']:
    with open(OUTPUT_FILE_NAME, 'a+') as file:
        print('Langauge: {} quantity: {}'.format(ln, quant),file=file)

    proj_dir = os.getcwd()
    path = proj_dir + '/data/embeddings/{}/200000/{}.emb.vec'.format('word2vec', ln)
    emb = load_vectors(path, quant)
    keys1 = list(emb.keys())
    path = proj_dir + '/data/embeddings/{}/200000/{}.emb.vec'.format('fasttext', ln)
    emb = load_vectors(path, quant)
    keys2 = list(emb.keys())
    calc_cover(keys1, keys2)
