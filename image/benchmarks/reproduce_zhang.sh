#!/bin/bash
#SBATCH --job-name="zhang"
#SBATCH  --gres=gpu:1
#SBATCH --partition=all
#SBATCH --wait-all-nodes=1
#SBATCH --output="slurm_reproduce_zhang.out"

python3 -u  benchmarks/reproduce_zhang.py
