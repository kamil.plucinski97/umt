#!/bin/bash
#SBATCH --job-name="umt_tr"
#SBATCH  --gres=gpu:1
#SBATCH --partition=all
#SBATCH --wait-all-nodes=1
#SBATCH --output="slurm_threhsold_test"

python3 -u benchmarks/reproduce_paper_threshold.py
