#!/bin/bash
#SBATCH --job-name="umt_test_k_10"
#SBATCH  --gres=gpu:1
#SBATCH --partition=all
#SBATCH --wait-all-nodes=1
#SBATCH --output="slurm_test_k_10_20.out"

python3 -u  benchmarks/test_k_10-25-100.py
