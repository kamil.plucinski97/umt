import collections
import os

import numpy

from scripts.unsupervised_learning import unsupervised_learning
from scripts.validate import BATCH_SIZE
from src.utils.config import Config
from src.utils.math_functions import length_normalize, np_, knn


def construct_mapping_dictionary(XWx, ZWz):
    knn_row_mean_similarity = np_.zeros(ZWz.shape[0])
    translation = list()
    certainty = list()
    for source_index in range(0, ZWz.shape[0], BATCH_SIZE):
        j = min(source_index + BATCH_SIZE, ZWz.shape[0])
        knn_row_mean_similarity[source_index:j] = knn(ZWz[source_index:j].dot(XWx.T), k=config.k, inplace=True)
    for source_index in range(0, ZWz.shape[0], BATCH_SIZE):
        j = min(source_index + BATCH_SIZE, XWx.shape[0])
        similarities = 2 * XWx[source_index:j].dot(ZWz.T) - knn_row_mean_similarity
        nn = np_.argpartition(similarities, -2, axis=1)[:, -2:]
        for row, group in enumerate(nn):
            certainty.append(similarities[row, group])
        # certainty.extend(similarities.max(axis=1).tolist())
        for k in range(j - source_index):
            translation.append(nn[k])
    return translation, certainty


def confirmed_mapping(XWx, ZWz):
    first_translation, first_certainty = construct_mapping_dictionary(XWx, ZWz)
    second_translation, second_certainty = construct_mapping_dictionary(ZWz, XWx)
    result_translation, result_certainty = list(), list()

    for word_index, mapping_group in enumerate(first_translation):
        for translation_index, mapping_index in enumerate(mapping_group):
            try:
                if word_index in second_translation[mapping_index]:
                    result_translation.append(mapping_index)
                    result_certainty.append(first_certainty[word_index][translation_index])
                    break
            except IndexError:
                pass
        else:
            translation_index = np_.argmax(first_certainty[word_index])
            mapping_index = mapping_group[translation_index]
            result_translation.append(mapping_index)
            result_certainty.append(first_certainty[word_index][translation_index])

    return result_translation, result_certainty


args = {
    'source': '.',
    'target': '.',
    'validation': None,
    'reverse': False,
    'embedding_size': 100000,
    'words_initialize': 4000,
    'words_learn': 20000,
    'dropout_wait': 50,
    'knn': 10,
    'keep_probability': 0.1,
    'output': './',
    'init_only': False,
    'init': 'classic',
    'init_time': 3600,
    'cost': 'classic',
    'cost_threshold': 100.0,
    'save_embeddings': False,
    'save_output': False
}
proj_dir = os.getcwd()
path = proj_dir + '/data/embeddings/{}/200000/{}.emb.vec'
v_path = proj_dir + '/data/dictionaries/{}-{}.test.txt'

emb = 'word2vec'
source = 'en'
for target in ['es', 'fi', 'de', 'it']:
    print(target)
    for _ in range(10):
        args['source'] = path.format(emb, source)
        args['target'] = path.format(emb, target)
        args['validation'] = v_path.format(source, target)

        config = Config(**args)
        trainer, keys = unsupervised_learning(config)

        XWx = length_normalize(trainer.XWx)
        ZWz = length_normalize(trainer.ZWz)

        source_dict_keys, target_dict_keys = keys

        source_word_to_index_dict = {word: i for i, word in enumerate(source_dict_keys)}
        target_word_to_index_dict = {word: i for i, word in enumerate(target_dict_keys)}

        validation_dict = trainer.create_validation_dict(config.validation_dict_path, source_word_to_index_dict,
                                                         target_word_to_index_dict, config.reverse_validation)
        source_words = list(validation_dict.keys())

        translation, _ = confirmed_mapping(XWx, ZWz)

        accuracy = numpy.mean([1 if translation[i] in validation_dict[i] else 0 for i in source_words])
        print('{} {}'.format(accuracy, trainer._result_config['learning_time']))
