import collections
import os

import numpy

from scripts.validate import BATCH_SIZE
from src.trainer.trainer import DTYPE, Trainer
from src.utils.config import Config
from src.utils.handling_data import load_vectors
from src.utils.math_functions import length_normalize, np_, knn


def unsupervised_learning(config):
    # np_.random.seed(0)
    # user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
    # print(user_paths)
    # print('Started with: {}'.format(config))

    source_dict = load_vectors(config.source_embeddings_path, config.embedding_size)
    target_dict = load_vectors(config.target_embeddings_path, config.embedding_size)

    source_dict_values = [value for _, value in source_dict.items()]
    target_dict_values = [value for _, value in target_dict.items()]

    source_dict_keys = list(source_dict.keys())
    target_dict_keys = list(target_dict.keys())

    source_word_to_index_dict = {word: i for i, word in enumerate(source_dict_keys)}
    target_word_to_index_dict = {word: i for i, word in enumerate(target_dict_keys)}

    X = np_.array(source_dict_values[0:config.embedding_size], dtype=DTYPE)
    Z = np_.array(target_dict_values[0:config.embedding_size], dtype=DTYPE)

    # SQUARED
    # squared_X = np_.square(X)
    # squared_Z = np_.square(Z)
    #
    # X = np_.hstack((X, squared_X))
    # Z = np_.hstack((Z, squared_Z))

    # REDUCED
    # pca = PCA(n_components=.9, svd_solver='full')
    # pca.fit(X)
    # n_components = pca.n_components_
    # print('Reduced to {} dimensions'.format(n_components))
    #
    # X = pca.transform(X)
    #
    # pca = PCA(n_components=n_components, svd_solver='full')
    # Z = pca.fit_transform(Z)

    trainer = Trainer(config, X, Z)
    trainer.initial_solution()

    if config.validation_dict_path is not None:
        trainer.validation = trainer.create_validation_dict(config.validation_dict_path, source_word_to_index_dict,
                                                            target_word_to_index_dict, config.reverse_validation)

    while True:
        obj, end = trainer.train_step()

        if config.validation_dict_path is not None:
            trainer.validate()

        if end:
            if config.save_output:
                trainer.save_output()
            if config.save_embeddings:
                trainer.save_results((source_dict_keys, target_dict_keys))
            break

    return trainer, (source_dict_keys, target_dict_keys)


args = {
    'source': '.',
    'target': '.',
    'validation': None,
    'reverse': False,
    'embedding_size': 200000,
    'words_initialize': 4000,
    'words_learn': 20000,
    'dropout_wait': 50,
    'knn': 10,
    'keep_probability': 1,
    'output': './',
    'init_only': False,
    'init': 'classic',
    'init_time': 3600,
    'cost': 'classic',
    'cost_threshold': 100.0,
    'save_embeddings': False,
    'save_output': False
}
proj_dir = os.getcwd()
path = proj_dir + '/data/embeddings/{}/200000/{}.emb.vec'
v_path = proj_dir + '/data/dictionaries/{}-{}.test.txt'

emb = 'word2vec'
source = 'en'
for target in ['es', 'fi', 'de', 'it']:
    print(target)
    for _ in range(10):
        args['source'] = path.format(emb, source)
        args['target'] = path.format(emb, target)
        args['validation'] = v_path.format(source, target)

        config = Config(**args)
        trainer, keys = unsupervised_learning(config)

        XWx = length_normalize(trainer.XWx)
        ZWz = length_normalize(trainer.ZWz)

        source_dict_keys, target_dict_keys = keys

        source_word_to_index_dict = {word: i for i, word in enumerate(source_dict_keys)}
        target_word_to_index_dict = {word: i for i, word in enumerate(target_dict_keys)}

        validation_dict = trainer.create_validation_dict(config.validation_dict_path, source_word_to_index_dict,
                                                         target_word_to_index_dict, config.reverse_validation)
        source_words = list(validation_dict.keys())

        translation = collections.defaultdict(int)
        knn_column_mean_similarity = np_.zeros(ZWz.shape[0])
        for i in range(0, ZWz.shape[0], BATCH_SIZE):
            j = min(i + BATCH_SIZE, ZWz.shape[0])
            knn_column_mean_similarity[i:j] = knn(ZWz[i:j].dot(XWx.T), k=config.k, inplace=True)
        for i in range(0, len(source_words), BATCH_SIZE):
            j = min(i + BATCH_SIZE, len(source_words))
            similarities = 2 * XWx[source_words[i:j]].dot(ZWz.T) - knn_column_mean_similarity
            nn = similarities.argmax(axis=1).tolist()
            for k in range(j - i):
                translation[source_words[i + k]] = nn[k]

        accuracy = numpy.mean([1 if translation[i] in validation_dict[i] else 0 for i in source_words])
        print('{} {}'.format(accuracy, trainer._result_config['learning_time']))
