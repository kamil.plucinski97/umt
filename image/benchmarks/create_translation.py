import collections
import os
import sys

import numpy

from scripts.unsupervised_learning import unsupervised_learning
from scripts.validate import BATCH_SIZE
from src.utils.config import Config
from src.utils.handling_data import create_directory_if_necessary
from src.utils.math_functions import length_normalize, np_, knn

args = {
    'source': '.',
    'target': '.',
    'validation': None,
    'reverse': False,
    'embedding_size': 200000,
    'words_initialize': 4000,
    'words_learn': 20000,
    'dropout_wait': 50,
    'knn': 10,
    'keep_probability': 0.1,
    'output': './',
    'init_only': False,
    'init': 'classic',
    'init_time': 3600,
    'cost': 'classic',
    'cost_threshold': 100.0,
    'save_embeddings': False,
    'save_output': False
}

if len(sys.argv) < 2:
    print("Provide embeddings paths")

source_emb_path = sys.argv[1]
target_emb_path = sys.argv[2]
validation_path = sys.argv[3] if len(sys.argv) == 4 else None
print(source_emb_path)
train_list = [
    [source_emb_path, target_emb_path, validation_path, False],
    [target_emb_path, source_emb_path, validation_path, True]
]

for source, target, validation_path, reverse_flag in train_list:
    print(source + ' ' + target)
    args['source'] = source
    args['target'] = target

    if validation_path:
        args['validation'] = validation_path
        args['reverse'] = reverse_flag

    config = Config(**args)

    trainer, keys = unsupervised_learning(config)

    XWx = length_normalize(trainer.XWx)
    ZWz = length_normalize(trainer.ZWz)

    if validation_path:
        source_dict_keys, target_dict_keys = keys

        source_word_to_index_dict = {word: i for i, word in enumerate(source_dict_keys)}
        target_word_to_index_dict = {word: i for i, word in enumerate(target_dict_keys)}

        validation_dict = trainer.create_validation_dict(config.validation_dict_path, source_word_to_index_dict,
                                                         target_word_to_index_dict, config.reverse_validation)
        source_words = list(validation_dict.keys())

    knn_row_mean_similarity = np_.zeros(ZWz.shape[0])
    translation = collections.defaultdict(int)
    certainty = list()
    for source_index in range(0, ZWz.shape[0], BATCH_SIZE):
        j = min(source_index + BATCH_SIZE, ZWz.shape[0])
        knn_row_mean_similarity[source_index:j] = knn(ZWz[source_index:j].dot(XWx.T), k=config.k, inplace=True)
    for source_index in range(0, ZWz.shape[0], BATCH_SIZE):
        j = min(source_index + BATCH_SIZE, ZWz.shape[0])
        similarities = 2 * XWx[source_index:j].dot(ZWz.T) - knn_row_mean_similarity
        nn = similarities.argmax(axis=1).tolist()
        certainty.extend(similarities.max(axis=1).tolist())
        for k in range(j - source_index):
            translation[source_index + k] = nn[k]

    if validation_path:
        accuracy = numpy.mean(
            [1 if translation[src_ind] in validation_dict[src_ind] else 0 for src_ind in list(validation_dict.keys())])
        print('{} {}'.format(accuracy, trainer._result_config['learning_time']))

    translated_words = list()
    min_c = min(certainty)
    max_c = max(certainty)
    for source_index, target_index in enumerate(translation.values()):
        c = (certainty[source_index] - min_c) / (max_c - min_c)
        translated_words.append(
            '{};{};{:.4f}'.format(source_dict_keys[source_index], target_dict_keys[target_index], c))

    result = '\n'.join(translated_words)
    t = './output/translations/'
    create_directory_if_necessary(t)
    acc = accuracy if validation_path else numpy.mean(certainty)
    trainer.save_results(keys, acc * 100)
    with open(t + '{}-{}-{:.2f}.txt'.format(trainer._result_config['source'], trainer._result_config['target'],
                                            acc * 100), "w") as text_file:
        text_file.write(result)
