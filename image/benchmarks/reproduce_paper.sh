#!/bin/bash
#SBATCH --job-name="umt"
#SBATCH  --gres=gpu:1
#SBATCH --partition=all
#SBATCH --wait-all-nodes=1
#SBATCH --output="slurm_reproduce_paperout"

python3 -u  benchmarks/reproduce_paper.py
