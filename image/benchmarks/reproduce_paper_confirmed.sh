#!/bin/bash
#SBATCH --job-name="umt_confirmed"
#SBATCH  --gres=gpu:1
#SBATCH --partition=all
#SBATCH --wait-all-nodes=1
#SBATCH --output="slurm_reproduce_paper_confirmed.out"

python3 -u  benchmarks/reproduce_paper_confirmed.py
