#!/bin/bash
#SBATCH --job-name="umt_csls"
#SBATCH  --gres=gpu:1
#SBATCH --partition=all
#SBATCH --wait-all-nodes=1
#SBATCH --output="slurm_reproduce_paper_csls_0.out"

python3 -u  benchmarks/k.py
