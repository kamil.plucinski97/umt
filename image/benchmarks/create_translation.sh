#!/bin/bash
#SBATCH --job-name="umt_translate"
#SBATCH  --gres=gpu:1
#SBATCH --partition=all
#SBATCH --wait-all-nodes=1
#SBATCH --output="slurm_translate_result.out"

python3 -u  benchmarks/create_translation.py $1 $2 $3
