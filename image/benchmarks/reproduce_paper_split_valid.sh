#!/bin/bash
#SBATCH --job-name="umt_spl_val"
#SBATCH  --gres=gpu:1
#SBATCH --partition=all
#SBATCH --wait-all-nodes=1
#SBATCH --output="slurm_reproduce_paperout_split_valid.out"

python3 -u  benchmarks/reproduce_paper_split_valid.py
