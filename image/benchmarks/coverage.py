import collections
from src.utils.handling_data import get_validation_data, load_vectors
import os

OUTPUT_FILE_NAME = 'output/tables_and_plots/coverage.txt'


def test_coverage(ln, source, quant):
    proj_dir = os.getcwd()
    dictionary_path = proj_dir + '/data/dictionaries/{}-{}.test.txt'.format(source, ln)
    reverse = False
    with open(dictionary_path) as file:
        validation = collections.defaultdict(set)
        for line in file:
            source_word, target_word = line.split()
            if reverse:
                source_word, target_word = target_word, source_word
            try:
                validation[source_word].add(target_word)
            except KeyError:
                pass
    source_keys_validation = set(list(validation.keys()))
    target_keys_validation = set([i for key in validation for i in validation[key]])

    def coverage_validation_by_embeddings(emb, source, ln):

        proj_dir = os.getcwd()
        path =proj_dir +'/data/embeddings/{}/200000/{}.emb.vec'.format(emb, 'en')
        embedding = load_vectors(path, quant)
        keys_source = set(embedding.keys()) & source_keys_validation
        path =proj_dir+ '/data/embeddings/{}/200000/{}.emb.vec'.format(emb, ln)
        embedding = load_vectors(path, quant)
        keys_target = set(embedding.keys()) & target_keys_validation

        result = 0
        for item, values in validation.items():
            for value in values:
                if item in keys_source and value in keys_target:
                    result += 1
                    break
        return result

    with open(OUTPUT_FILE_NAME, 'a+') as file:
        print('Coverage using {} words: word2vec:{:.2%}, fasttext:{:.2%}'.format(
            quant,
            coverage_validation_by_embeddings('word2vec', source, ln) / len(source_keys_validation),
            coverage_validation_by_embeddings('fasttext', source, ln) / len(source_keys_validation)),
            file=file
        )


sources = ['en', 'en', 'en', 'en', 'en', 'es', 'en']
targets = ['it', 'de', 'fi', 'es', 'pl', 'pl', 'cs']
for source, ln in zip(sources, targets):
    for q in [4000, 20000, 200000]:
        with open(OUTPUT_FILE_NAME, 'a+') as file:
            print('Source: {}, tagrget:{}, quantity:{}'.format(source, ln, q), file=file)
        test_coverage(ln, source, q)
