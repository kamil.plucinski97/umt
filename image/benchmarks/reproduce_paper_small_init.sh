#!/bin/bash
#SBATCH --job-name="umt_small_init"
#SBATCH  --gres=gpu:1
#SBATCH --partition=all
#SBATCH --wait-all-nodes=1
#SBATCH --output="slurm_small_init"

python3 -u benchmarks/reproduce_paper_small_init.py $1 $2
