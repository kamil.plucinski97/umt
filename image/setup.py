from setuptools import setup, find_packages

setup(
    name='umt',
    version='0.1',
    py_modules=['src'],
    packages=find_packages(),
    install_requires=[
        'Click',
        'seaborn',
        'scikit-learn',
        'numpy==1.16.0',
        'matplotlib',
        'pandas',
    ],
    entry_points='''
        [console_scripts]
        umt=umt:cli
    ''',
)
