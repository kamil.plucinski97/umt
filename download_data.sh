ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DATA="$ROOT"

echo "Download process was started"

wget -nv "https://drive.man.poznan.pl/s/MxybgqMiNLpsWCa/download" -O "input.tar.gz" --no-check-certificate -q --show-progress
tar -xzvf "input.tar.gz"
rm "input.tar.gz"

echo "Finished"
